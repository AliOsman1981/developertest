﻿using System.Data.Entity;
using DeveloperTest.Infrastructure.Data;
using DeveloperTest.UnitTests.Helpers;
using Xunit;

namespace DeveloperTest.UnitTests.WhenUsingDataContext
{
    public class AndType
    {
        [Fact]
        public void ItShouldBeOfTypeDbContext()
        {
            var ctx = new DataContext(RandomValues.RandomString(5));

            Assert.IsAssignableFrom<DbContext>(ctx);
        }
    }
}
