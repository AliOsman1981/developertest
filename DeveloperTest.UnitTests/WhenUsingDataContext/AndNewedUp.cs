﻿using DeveloperTest.Infrastructure.Data;
using DeveloperTest.UnitTests.Helpers;
using Xunit;

namespace DeveloperTest.UnitTests.WhenUsingDataContext
{
    public class AndNewedUp
    {
        [Fact]
        public void ItShouldSetProxyCreationEnabledToFalse()
        {
            var ctx = new DataContext(RandomValues.RandomString(5));

            Assert.False(ctx.Configuration.ProxyCreationEnabled);
        }
    }
}
