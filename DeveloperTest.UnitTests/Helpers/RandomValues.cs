﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperTest.UnitTests.Helpers
{
    public class RandomValues
    {
        public static string RandomString(int length)
        {
            var guid = Guid.NewGuid();

            return guid.ToString().Substring(0, length);
        }
        public static int RandomInteger()
        {
            var random = new Random();

            return random.Next(0, 10);
        }
        public static DateTime RandomDate()
        {
            var startDate = new DateTime(1950, 1, 1);
            var random = new Random();

            var range = (DateTime.Today - startDate).Days;
            return startDate.AddDays(random.Next(range));
        }

        public static int GetNext(int min, int max)
        {
            var random = new Random();

            return random.Next(min, max);
        }
    }
}
