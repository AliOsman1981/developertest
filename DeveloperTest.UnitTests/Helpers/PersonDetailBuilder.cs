﻿using System;
using DeveloperTest.Core;

namespace DeveloperTest.UnitTests.Helpers
{
    public class PersonDetailBuilder
    {
        private int? _id;
        private DateTime? _birthDate;
        private string _emailAddress;

        public PersonDetailBuilder WithId(int id)
        {
            _id = id;
            return this;
        }
        public PersonDetailBuilder WithBirthDate(DateTime birthDate)
        {
            _birthDate = birthDate;
            return this;
        }
        public PersonDetailBuilder WithEmailAddress(string emailAddress)
        {
            _emailAddress = emailAddress;
            return this;
        }
        public PersonDetail Build()
        {
            return new PersonDetail
            {
                Id = _id ?? RandomValues.RandomInteger(),
                BirthDate = _birthDate ?? RandomValues.RandomDate(),
                EmailAddress = _emailAddress
            };
        }
    }
}
