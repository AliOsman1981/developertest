﻿using System;
using DeveloperTest.Core;
using Xunit;

namespace DeveloperTest.UnitTests.Entities.WhenUsingPersonDetails
{
    public class AndNewedUp
    {
        [Fact]
        public void IdShouldBeNegativeOne()
        {
            Assert.Equal(-1, new PersonDetail().Id);
        }
        public void PersonIdShouldBeNegativeOne()
        {
            Assert.Equal(-1, new PersonDetail().PersonId);
        }
        public void BirthdateShouldBeMinDate()
        {
            Assert.Equal(DateTime.MinValue, new PersonDetail().BirthDate);
        }
        public void EmailAddressShouldBeEmpty()
        {
            Assert.Empty(new PersonDetail().EmailAddress);
        }
    }
}
