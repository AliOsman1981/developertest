﻿using DeveloperTest.Core;
using DeveloperTest.Core.Contracts;
using Xunit;

namespace DeveloperTest.UnitTests.Entities.WhenUsingPerson
{
    public class AndType
    {
        [Fact]
        public void ItShouldImplementIEntity()
        {
            var person = new Person();

            Assert.IsAssignableFrom<IEntity>(person);
        }
    }
}
