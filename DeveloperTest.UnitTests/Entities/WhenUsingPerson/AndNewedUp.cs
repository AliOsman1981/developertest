﻿using DeveloperTest.Core;
using Xunit;

namespace DeveloperTest.UnitTests.Entities.WhenUsingPerson
{
    public class AndNewedUp
    {
        [Fact]
        public void IdShouldBeNegativeOne()
        {
            Assert.Equal(-1, new Person().Id);
        }
        public void FirstNameShouldBeEmpty()
        {
            Assert.Empty(new Person().FirstName);
        }
        public void MiddleNameShouldBeEmpty()
        {
            Assert.Empty(new Person().MiddleName);
        }
        public void LastNameShouldBeEmpty()
        {
            Assert.Empty(new Person().LastName);
        }

        public void PersonDetailsShouldBeEmptyCollection()
        {
            var person = new Person();

            Assert.Equal(0, person.PersonDetails.Count);
        }
    }
}
