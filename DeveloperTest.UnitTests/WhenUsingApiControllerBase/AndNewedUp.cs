﻿using DeveloperTest.Web.Controllers;
using Xunit;

namespace DeveloperTest.UnitTests.WhenUsingApiControllerBase
{
    public class AndNewedUp
    {
        [Fact]
        public void PersonRepositoryShouldNotBeNull()
        {
            var controllerBase = new ApiControllerBase();

            Assert.NotNull(controllerBase.PersonRepository);
        }
    }
}
