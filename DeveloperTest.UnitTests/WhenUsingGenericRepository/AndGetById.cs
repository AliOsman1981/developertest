﻿using System.Collections.Generic;
using System.Linq;
using DeveloperTest.Core;
using DeveloperTest.Infrastructure.Data.Repositories;
using DeveloperTest.UnitTests.Helpers;
using Rhino.Mocks;
using Xunit;

namespace DeveloperTest.UnitTests.WhenUsingGenericRepository
{
    public class AndGetAll
    {
        [Fact]
        public void ItShouldReturnItems()
        {
            var count = RandomValues.GetNext(2, 5);

            var builder = new PersonBuilder();

            var repo = MockRepository.GenerateMock<IRepository<Person>>();

            var personCollection = new List<Person>();

            for (int i = 0; i < count; i++)
            {
                personCollection.Add(builder.WithId(i).Build());   
            }

            repo.Stub(x => x.GetById(Arg<int>.Is.Equal(count)))
                .Return(personCollection.FirstOrDefault(x => x.Id == count));

            Assert.Equal(count, repo.GetById(count).Id);
        }
    }
}
