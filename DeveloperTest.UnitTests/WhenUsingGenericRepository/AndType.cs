﻿using System;
using System.Data.Entity;
using DeveloperTest.Core;
using DeveloperTest.Infrastructure.Data;
using DeveloperTest.Infrastructure.Data.Repositories;
using DeveloperTest.UnitTests.Helpers;
using Xunit;

namespace DeveloperTest.UnitTests.WhenUsingGenericRepository
{
    public class AndType
    {
        [Fact]
        public void ItShouldImplementIDisposable()
        {
            var repo = new Repository<Person>(null);

            Assert.IsAssignableFrom<IDisposable>(repo);
        }
    }
}
