﻿using System;
using DeveloperTest.Core;
using DeveloperTest.Infrastructure.Data.Repositories;
using Xunit;

namespace DeveloperTest.UnitTests.WhenUsingGenericRepository
{
    public class AndNewedUp
    {
        [Fact]
        public void ItShouldThrowExceptionWhenDataContextIsNull()
        {
            Assert.Throws<NullReferenceException>(() => new Repository<Person>(null));
        }
    }
}
