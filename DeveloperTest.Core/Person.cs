using System.Collections;
using DeveloperTest.Core.Contracts;
using System.Collections.Generic;

namespace DeveloperTest.Core
{
    public class Person : IEntity
    {
        public Person()
        {
            Id = -1;
            FirstName = string.Empty;
            MiddleName = string.Empty;
            LastName = string.Empty;

            PersonDetails = new HashSet<PersonDetail>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public virtual ICollection<PersonDetail> PersonDetails { get; set; }
    }
}