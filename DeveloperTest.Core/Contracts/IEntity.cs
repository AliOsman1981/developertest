﻿namespace DeveloperTest.Core.Contracts
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}