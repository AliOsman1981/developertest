﻿using System;
using DeveloperTest.Core.Contracts;

namespace DeveloperTest.Core
{
    public class PersonDetail : IEntity
    {
        public PersonDetail()
        {
            Id = -1;
            PersonId = -1;
            BirthDate = DateTime.MinValue;
            EmailAddress = string.Empty;
        }

        public int Id { get; set; }
        public int PersonId { get; set; }
        public DateTime BirthDate { get; set; }
        public string EmailAddress { get; set; }

        public virtual Person Person { get; set; }
    }
}