﻿using System.Data.Entity;
using DeveloperTest.Core;

namespace DeveloperTest.Infrastructure.Data
{
    public class DataContext : DbContext
    {
        public DataContext(string connString) : base(connString)
        {
            Database.SetInitializer<DataContext>(null);

            this.Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PersonConfigurations());
            modelBuilder.Configurations.Add(new PersonDetailConfigurations());

            base.OnModelCreating(modelBuilder);
        }

        public IDbSet<Person> Persons { get; set; }
        public IDbSet<PersonDetail> PersonDetails { get; set; } 
    }
}
