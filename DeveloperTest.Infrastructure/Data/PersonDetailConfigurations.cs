﻿using System.Data.Entity.ModelConfiguration;
using DeveloperTest.Core;

namespace DeveloperTest.Infrastructure.Data
{
    public class PersonDetailConfigurations : EntityTypeConfiguration<PersonDetail>
    {
        public PersonDetailConfigurations()
        {
            ToTable("PersonDetail");
            HasKey(x => x.Id);

            HasRequired(x => x.Person)
                .WithMany(x => x.PersonDetails)
                .HasForeignKey(x => x.PersonId);
        }
    }
}
