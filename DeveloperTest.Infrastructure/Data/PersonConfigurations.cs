﻿using System.Data.Entity.ModelConfiguration;
using DeveloperTest.Core;

namespace DeveloperTest.Infrastructure.Data
{
    public class PersonConfigurations : EntityTypeConfiguration<Person>
    {
        public PersonConfigurations()
        {
            ToTable("Person");
            HasKey(x => x.Id);

            HasMany(x => x.PersonDetails)
                .WithRequired(x => x.Person);
        }
    }
}
