﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace DeveloperTest.Infrastructure.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private DataContext _ctx;
        private readonly IDbSet<T> _dbSet; 
        public Repository(DataContext ctx)
        {
            if (ctx == null)
            {
                throw new NullReferenceException("ctx");
            }
            _ctx = ctx;
            _dbSet = _ctx.Set<T>();
        }
        public IEnumerable<T> GetAll()
        {
            return _dbSet;
        }

        public T GetById(int id)
        {
            return _dbSet.Find(id);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _ctx.Dispose();
                _ctx = null;
            }
        }

    }
}
