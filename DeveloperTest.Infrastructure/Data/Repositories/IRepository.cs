﻿using System;
using System.Collections.Generic;

namespace DeveloperTest.Infrastructure.Data.Repositories
{
    public interface IRepository<T> : IDisposable where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
    }
}
