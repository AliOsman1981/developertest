﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.factory('PersonService', [
        '$http', '$q', function ($http, $q) {

            var serviceEndPoint = '/api/person';

            var factory = {};

            var getAllPersons = function () {
                var deferred = $q.defer();
                $http.get(serviceEndPoint).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }

            var savePerson = function (person) {
                var deferred = $q.defer();
                $http.post(serviceEndPoint, person).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }

            var calculateAge = function (dateOfBirth) {
                var deferred = $q.defer();
                $http.get(serviceEndPoint + '/CalculateAge?dateOfBirth=' + dateOfBirth).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }

            factory.getAllPersons = getAllPersons;
            factory.savePerson = savePerson;
            factory.calculateAge = calculateAge;

            return factory;
        }
    ]);
})();