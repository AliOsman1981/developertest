﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('PersonController', ['$scope', 'PersonService', function ($scope, personService) {
        var errors = [];

        activate();

        $scope.addNew = addNew;
        $scope.onRowClick = onRowClick;
        $scope.onDateOfBirthChanged = onDateOfBirthChanged;
        $scope.rowSelected = false;

        function activate() {
            $scope.errors = errors;

            personService.getAllPersons().then(function (results) {
                console.log(results);
                $scope.persons = results;
            },
            function (errors) {
                console.log('errors', errors);
            });
        }


        function onRowClick(person) {
            $scope.rowSelected = !person.showDetails;
            person.showDetails = !person.showDetails;

            if (person.showDetails === false) return;

            getDateOfBirth(person);

            $scope.selectedPerson = person;

            console.log(person);
        }

        function onDateOfBirthChanged() {
            getDateOfBirth($scope.selectedPerson);
        }

        function addNew() {
            $scope.selectedPerson = {
                Id: -1
            }
        }

        function getDateOfBirth(person) {
            personService.calculateAge(person.BirthDate).then(function (results) {
                console.log(results);
                    person.Age = results;
                },
            function (errors) {
                console.log('errors', errors);
            });
        }
    }]);
})();
