﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using DeveloperTest.Core;

namespace DeveloperTest.Web.Controllers
{
    public class PersonController : ApiControllerBase
    {
        public IEnumerable<object> Get()
        {
            var person = (from p in PersonRepository.GetAll()
                join pd in PersonDetailRepository.GetAll() on p.Id equals pd.PersonId
                select
                    new
                    {
                        Id = p.Id,
                        FirstName = p.FirstName,
                        MiddleName = p.MiddleName,
                        LastName = p.LastName,
                        BirthDate = pd.BirthDate,
                        EmailAddress = pd.EmailAddress
                    })
                .ToList();
            return person;
        }

        public void Post([FromBody]object value)
        {

        }

        [HttpGet, ActionName("CalculateAge")]
        public int CalculateAge(DateTime dateOfBirth)
        {
            int age = 0;

            var span = DateTime.Today.Subtract(dateOfBirth);

            age = span.Days / 365;

            return age;
        }
    }
}