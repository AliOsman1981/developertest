﻿using System.Configuration;
using System.Web.Http;
using DeveloperTest.Core;
using DeveloperTest.Infrastructure.Data;
using DeveloperTest.Infrastructure.Data.Repositories;

namespace DeveloperTest.Web.Controllers
{
    public class ApiControllerBase : ApiController
    {
        public ApiControllerBase()
        {
            var connString = ConfigurationManager.ConnectionStrings["DeveloperTest"].ConnectionString;

            var ctx = new DataContext(connString);
            PersonRepository = new Repository<Person>(ctx);
            PersonDetailRepository = new Repository<PersonDetail>(ctx);
        }
        public IRepository<Person> PersonRepository { get; private set; }

        public IRepository<PersonDetail> PersonDetailRepository { get; private set; } 
    }
}